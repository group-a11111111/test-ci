# Create build stage based on buster image
FROM golang:1.20.5 AS builder
# Create working directory under /app
WORKDIR /app
# Copy over all go config (go.mod, go.sum etc.)
COPY go.* ./
# Install any required modules
RUN go mod download
# Copy over Go source code
COPY *.go ./
# Run the Go build and output binary under test-ci
RUN go build -o /test-ci
# Run the app binary when we run the container
ENTRYPOINT ["/test-ci"]